/**
 * Created by jorge-jr on 17/06/15.
 */
function replace(str){
    for(var i=0;i<str.length;i++){
        str=str.replace(" ","_");
    }
    return str;
}
function EscreverComando() {
    $('.btn-send').button('loading');
    ok=false;
    var nomeLoja=replace(document.PSInstaller.nomeLoja.value);

    var primeiroNome=replace(document.PSInstaller.primeiroNome.value);
    var sobreNome=replace(document.PSInstaller.sobreNome.value);
    
    var Pasta=replace($('#folder').val());
    
    var senhaLoja=document.PSInstaller.senhaLoja.value;
    var emailAdmin=document.PSInstaller.emailAdmin.value;
    var dominio=document.PSInstaller.dominio.value;
    var bdServidor=document.PSInstaller.bdServidor.value;
    var bdNome=document.PSInstaller.bdNome.value;
    var bdUsuario=document.PSInstaller.bdUsuario.value;
    var bdSenha=document.PSInstaller.bdSenha.value;
    var prefixo=document.PSInstaller.prefixo.value;
    var linguagem=document.PSInstaller.linguagem.checked;
    var ftpHost=$('#ftpHost').val();
    var ftpUser=$('#ftpUser').val();
    var ftpPass=$('#ftpPass').val();
    if (nomeLoja!="") {
        document.getElementById('nomeLojaCampo').className="form-group has-success";


        document.getElementById('spannomeLoja').className="help-block";
        document.getElementById('spannomeLoja').innerHTML="Tudo OK";

    }
    
    if (Pasta) {
        $('#divFolder').attr('class',"form-group has-success");
        $('#spanFolder').attr('class',"help-block");
        $('#spanFolder').html("Tudo OK");

    }
    if (primeiroNome!="") {
        document.getElementById('primeiroNomeCampo').className="form-group has-success";


        document.getElementById('spanprimeiroNome').className="help-block";
        document.getElementById('spanprimeiroNome').innerHTML="Tudo OK";

    }
    if (sobreNome!="") {
        document.getElementById('sobreNomeCampo').className="form-group has-success";


        document.getElementById('spansobreNome').className="help-block";
        document.getElementById('spansobreNome').innerHTML="Tudo OK";

    }
    if (senhaLoja!="") {
        document.getElementById('senhaLojaCampo').className="form-group has-success";


        document.getElementById('spansenhaLoja').className="help-block";
        document.getElementById('spansenhaLoja').innerHTML="Tudo OK";

    }
    if (emailAdmin!="") {
        document.getElementById('emailAdminCampo').className="form-group has-success";


        document.getElementById('spanemailAdmin').className="help-block";
        document.getElementById('spanemailAdmin').innerHTML="Tudo OK";

    }
    if (dominio!="") {
        document.getElementById('dominioCampo').className="form-group has-success";


        document.getElementById('spandominio').className="help-block";
        document.getElementById('spandominio').innerHTML="Tudo OK";
        dominio="www."+document.PSInstaller.dominio.value;

    }
    if (bdServidor!="") {
        document.getElementById('bdServidorCampo').className="form-group has-success";
        document.getElementById('spanbdServidor').innerHTML="Tudo OK";
        document.getElementById('spanbdServidor').className="help-block";
        bdServidor=document.PSInstaller.bdServidor.value+".prv.f1.k8.com.br";

    }
    if (bdNome!="") {
        document.getElementById('bdNomeCampo').className="form-group has-success";
        document.getElementById('spanbdNome').innerHTML="Tudo OK";
        document.getElementById('spanbdNome').className="help-block";

    }
    if (bdUsuario!="") {
        document.getElementById('bdUsuarioCampo').className="form-group has-success";
        document.getElementById('spanbdUsuario').innerHTML="Tudo OK";
        document.getElementById('spanbdUsuario').className="help-block";

    }
    if (bdSenha!="") {
        document.getElementById('bdSenhaCampo').className="form-group has-success";
        document.getElementById('spanbdSenha').innerHTML="Tudo OK";
        document.getElementById('spanbdSenha').className="help-block";

    }
    if (prefixo!="") {
        document.getElementById('prefixoCampo').className="form-group has-success";
        document.getElementById('spanprefixo').innerHTML="Tudo OK";
        document.getElementById('spanprefixo').className="help-block";

    }
    if (ftpHost!="") {
        $('#divftpHost').attr('class',"form-group has-success");
        $('#spanftpHost').html("Tudo OK");
        $('#spanftpHost').attr('class','help-block');

    }
    if (ftpUser!="") {
        $('#divftpUser').attr('class',"form-group has-success");
        $('#spanftpUser').html("Tudo OK");
        $('#spanftpUser').attr('class','help-block');
    }
    if (ftpPass!="") {
        $('#divftpPass').attr('class',"form-group has-success");
        $('#spanftpPass').html("Tudo OK");
        $('#spanftpPass').attr('class','help-block');
    }
    
    /* Fim Tudo OK/*

     */


    if (nomeLoja=="") {
        document.getElementById('nomeLojaCampo').className="form-group has-error";


        document.getElementById('spannomeLoja').className="help-block";
        document.getElementById('spannomeLoja').innerHTML="Por favor, corrija o erro";

    }
    else if (primeiroNome=="") {
        document.getElementById('primeiroNomeCampo').className="form-group has-error";


        document.getElementById('spanprimeiroNome').className="help-block";
        document.getElementById('spanprimeiroNome').innerHTML="Por favor, corrija o erro";

    }
    else if (sobreNome=="") {
        document.getElementById('sobreNomeCampo').className="form-group has-error";


        document.getElementById('spansobreNome').className="help-block";
        document.getElementById('spansobreNome').innerHTML="Por favor, corrija o erro";

    }
    else if (senhaLoja=="") {
        document.getElementById('senhaLojaCampo').className="form-group has-error";


        document.getElementById('spansenhaLoja').className="help-block";
        document.getElementById('spansenhaLoja').innerHTML="Por favor, corrija o erro";

    }
    else if (emailAdmin=="") {
        document.getElementById('emailAdminCampo').className="form-group has-error";


        document.getElementById('spanemailAdmin').className="help-block";
        document.getElementById('spanemailAdmin').innerHTML="Por favor, corrija o erro";

    }
    else if (dominio=="") {
        document.getElementById('dominioCampo').className="form-group has-error";


        document.getElementById('spandominio').className="help-block";
        document.getElementById('spandominio').innerHTML="Por favor, corrija o erro";

    }
    else if (bdServidor=="") {
        document.getElementById('bdServidorCampo').className="form-group has-error";
        document.getElementById('spanbdServidor').innerHTML="Por favor, corrija o erro";
        document.getElementById('spanbdServidor').className="help-block";

    }
    else if (bdNome=="") {
        document.getElementById('bdNomeCampo').className="form-group has-error";
        document.getElementById('spanbdNome').innerHTML="Por favor, corrija o erro";
        document.getElementById('spanbdNome').className="help-block";

    }
    else if (bdUsuario=="") {
        document.getElementById('bdUsuarioCampo').className="form-group has-error";
        document.getElementById('spanbdUsuario').innerHTML="Por favor, corrija o erro";
        document.getElementById('spanbdUsuario').className="help-block";

    }
    else if (bdSenha=="") {
        document.getElementById('bdSenhaCampo').className="form-group has-error";
        document.getElementById('spanbdSenha').innerHTML="Por favor, corrija o erro";
        document.getElementById('spanbdSenha').className="help-block";

    }
    else if (prefixo=="") {
        document.getElementById('prefixoCampo').className="form-group has-error";
        document.getElementById('spanprefixo').innerHTML="Por favor, corrija o erro";
        document.getElementById('spanprefixo').className="help-block";

    }
    else if (ftpHost=="") {
        $('#divftpHost').attr('class',"form-group has-error");
        $('#spanftpHost').html("Por favor, corrija o erro");
        $('#spanftpHost').attr('class','help-block');

    }
    else if (ftpUser=="") {
        $('#divftpUser').attr('class',"form-group has-error");
        $('#spanftpUser').html("Por favor, corrija o erro");
        $('#spanftpUser').attr('class','help-block');
    }
    else if (ftpPass=="") {
        $('#divftpPass').attr('class',"form-group has-error");
        $('#spanftpPass').html("Por favor, corrija o erro");
        $('#spanftpPass').attr('class','help-block');
    }


    else{
        ok=true;
        if (linguagem){
            comando='php index_cli.php --name='+nomeLoja+' --firstname='+primeiroNome+' --lastname='+sobreNome+' --password='+senhaLoja+' --email='+emailAdmin+' --domain='+dominio+' --prefix='+prefixo+'_ --language=pt --db_server=mysql'+bdServidor+' --db_name='+bdNome+' --db_user='+bdUsuario+' --db_password='+bdSenha;
            /* php index_cli.php --name=Nome_da_loja --firstname=Nome_do_cliente --lastname=Sobrenome_do_cliente --password=Senha_da_loja --email=Email_Acesso_Admin --language=pt-br --domain=www.dominio_do_cliente.com.br --db_server=Servidor_MySQl --db_name=Nome_do_banco --db_user=Usuario_do_banco --db_password=Senha_do_Banco
             */
        }
        else{

            comando='php index_cli.php --name='+nomeLoja+' --firstname='+primeiroNome+' --lastname='+sobreNome+' --password='+senhaLoja+' --email='+emailAdmin+' --domain='+dominio+' --prefix='+prefixo+'_ --db_server=mysql'+bdServidor+' --db_name='+bdNome+' --db_user='+bdUsuario+' --db_password='+bdSenha;
        }
        $.post("ajax/proccess.php",{"comando": comando,"ftphost": ftpHost,"ftplogin": ftpUser,"ftppass": ftpPass,"folder": $('#folder').val()},function(data){
            $('#result').html(data);
            $('.btn-send').button('reset');
        });
    }
    if(!ok){
    $('.btn-send').button('reset');
    }
}
$(document).ready(function () {
    $('#result').css('margin-top','20px');
});

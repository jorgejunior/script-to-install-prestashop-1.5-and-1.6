<?php

/**
 * Created by PhpStorm.
 * User: jorge-jr
 * Date: 27/01/16
 * Time: 08:23
 */
class ftp
{
    private $ftp;
    function connect($host){
        if(ftp_connect($host)){
            $this->ftp=ftp_connect($host);
            return true;
        }
        else{
            echo "Erro ao se conectar com o servidor";
            return false;
        }
    }
    function ftplogin($user,$pass){
        if(!ftp_login($this->ftp,$user,$pass)){
            echo "Erro ao efetuar o login no FTP";
            return false;
        }
        return true;
    }
    function put($file,$filename){
        if(ftp_put($this->ftp,$filename,$file,FTP_BINARY)){
            return true;
        }
        else{
            return false;
        }
    }
    function mkdir($dir){
        return ftp_mkdir($this->ftp,$dir);
    }
}

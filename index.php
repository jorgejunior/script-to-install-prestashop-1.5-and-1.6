<!doctype html>
<html lang="PT-BR">
<head>
    <meta charset="UTF-8">
    <title>Prestashop by terminal</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/cipt.js"></script>
    <link rel="stylesheet" href="css/sweetalert.css">
    <script src="js/sweetalert.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form onsubmit="return false" name="PSInstaller" class="form-horizontal">
                <div class="form-group" id="divftpHost"><label for="" class="control-label">Host de FTP: </label>
                    <input type="text"  name="ftpHost" id="ftpHost" class="form-control">
                    <span class="hidden" id="spanftpHost">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="divftpUser"><label for="" class="control-label">Usuário de FTP: </label>
                    <input type="text"  name="ftpUser" id="ftpUser" class="form-control">
                    <span class="hidden" id="spanftpUser">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="divftpPass"><label for="" class="control-label">Senha de FTP: </label>
                    <input type="password"  name="ftpPass" id="ftpPass" class="form-control">
                    <span class="hidden" id="spanftpPass">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="divFolder"><label for="" class="control-label">Diretório de instalação: </label>
                    <input type="text"  name="folder" id="folder" class="form-control">
                <span class="hidden" id="spanFolder">Por favor, corrija o erro</span>

                </div>
                <div class="form-group" id="nomeLojaCampo"><label for="" class="control-label">Nome da loja: </label>
                    <input type="text"  name="nomeLoja" id="nomeLoja" class="form-control">
                    <span class="hidden" id="spannomeLoja">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="primeiroNomeCampo"><label for="" class="control-label">Primeiro nome: </label>
                    <input type="text"  name="primeiroNome" id="primeiroNome" class="form-control">
                    <span class="hidden" id="spanprimeiroNome">Por favor, corrija o erro</span>

                </div>
                <div class="form-group" id="sobreNomeCampo"><label for="" class="control-label">Sobrenome: </label>
                    <input type="text"  name="sobreNome" id="sobreNome" class="form-control">
                    <span class="hidden" id="spansobreNome">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="senhaLojaCampo"><label for="" class="control-label">Senha da loja: </label>
                    <input type="text"  name="senhaLoja" id="senhaLoja" class="form-control">
                    <span class="hidden" id="spansenhaLoja">Por favor, corrija o erro</span>

                </div>
                <div class="form-group" id="emailAdminCampo"><label for="" class="control-label">E-mail de acesso:</label>
                    <input type="text"  name="emailAdmin" id="emailAdmin" class="form-control">
                    <span class="hidden" id="spanemailAdmin">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="dominioCampo"><label for="" class="control-label">Dom&iacute;nio:</label>
                    <div class="input-group">
                        <span class="input-group-addon">www.</span>
                        <input type="text"  name="dominio" id="dominio" class="form-control"></div>

                    <span class="hidden" id="spandominio">Por favor, corrija o erro</span>

                </div>
                <div class="form-group" id="bdServidorCampo"><label for="" class="control-label">Servidor de banco de dados:</label>
                    <div class="input-group">
                        <span class="input-group-addon">mysql</span>
                        <input type="number"  name="bdServidor" id="bdServidor" class="form-control">
                        <span class="input-group-addon">.prv.f1.k8.com.br</span></div>
                    <span class="hidden" id="spanbdServidor">Por favor, corrija o erro</span>

                </div>
                <div class="form-group" id="bdNomeCampo"><label for="" class="control-label">Nome do banco de dados: </label>
                    <input type="text"  name="bdNome" id="bdNome" class="form-control">
                    <span class="hidden" id="spanbdNome">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="bdUsuarioCampo"><label for="" class="control-label">Usu&aacute;rio do banco de dados: </label>

                    <input type="text"  name="bdUsuario" id="bdUsuario" class="form-control">
                    <span class="hidden" id="spanbdUsuario">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="bdSenhaCampo"><label for="" class="control-label">Senha do banco de dados: </label>
                    <input type="text"  name="bdSenha" id="bdSenha" class="form-control">
                    <span class="password" id="spanbdSenha">Por favor, corrija o erro</span>
                </div>


                <div class="form-group" id="prefixoCampo"><label for="" class="control-label">Prefixo das tabelas: </label>
                    <div class="input-group">
                        <input type="text"  name="prefixo" id="prefixo" class="form-control" value="ps"><span class="input-group-addon">_</span>
                    </div>

                    <span class="hidden" id="spanprefixo">Por favor, corrija o erro</span>
                </div>
                <div class="form-group" id="linguagemCampo">
                    <label for="linguagem" class="switch">Portugues <input type="checkbox"  name="linguagem" id="linguagem">
                        <span></span>
                    </label>
                </div>
                <input type="button" onclick="EscreverComando()" value="Enviar arquivo" class="btn btn-success btn-block btn-send" data-loading-text="Enviando">
                <div id="result" style="margin-top: 20px">
                    
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>

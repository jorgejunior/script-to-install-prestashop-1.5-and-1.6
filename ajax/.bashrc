# .bashrc

# User specific aliases and functions

# Script Instalação Prestashop 1.6
# Yuri Brandão e Jorge Junior
prestashop16(){
	clear
#inserir comando do caminho da pasta EX: PASTA=~/www ou PASTA=~/www/loja

	mkdir ~/www/dasdasd >> ~/tmp/presta.log;

	cd ~;
	echo "Instalação do Prestashop 1.6"
	echo "Descompactando..."
	unzip -o prestashop_1.6.1.4_pt.zip >> ~/tmp/presta.log;
	mv -f prestashop/* ~/www/dasdasd >> ~/tmp/presta.log;
#Permissões
	echo "Ajustando Permissões..."
	chmod -R 755 ~/www/dasdasd >> ~/tmp/presta.log;
	cd ~/www/dasdasd/install;
	echo "Instalando... Por gentileza, aguarde alguns minutos."
# inserir comando php
php index_cli.php --name=asdasd --firstname=asdasd --lastname=asd --password=asdas --email=dasdas --domain=www.dasdasda --prefix=ps_ --db_server=mysql123.prv.f1.k8.com.br --db_name=asdasdasasd --db_user=asdasdas --db_password=dasdsdsa
#Ajustar Instação
	cd ~/www/dasdasd;
	rm -rf install/;
	rm -rf docs/;
	rm -rf ~/prestashop*;
	rm -rf ~/Install_PrestaShop.html;
	mv admin/ administracao/;
	chmod -R 755 ~/www/dasdasd;
	chmod -R 777 modules/ >> ~/tmp/presta.log;
	chmod -R 777 override/ >> ~/tmp/presta.log;
	chmod -R 777 cache/ >> ~/tmp/presta.log;
	chmod -R 777 img/ >> ~/tmp/presta.log;
}
+sed -i 4,38d ~/.bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

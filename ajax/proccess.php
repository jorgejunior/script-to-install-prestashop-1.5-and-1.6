<?php
$comando=$_POST['comando'];
$ftphost=$_POST['ftphost'];
$ftplogin=$_POST['ftplogin'];
$ftppass=$_POST['ftppass'];
require_once '../class/class.ftp.php';
$tmpcontent=file_get_contents("ftp://$ftplogin:$ftppass@$ftphost/.bashrc");
$PASTA=empty($_POST['folder'])?'~/www':'~/www'.DIRECTORY_SEPARATOR.$_POST['folder'];
$p='./www/'.$_POST['folder'];
$content="# .bashrc

# User specific aliases and functions

# Script Instalação Prestashop 1.6
# Yuri Brandão e Jorge Junior
prestashop16(){
	clear
#inserir comando do caminho da pasta EX: PASTA=~/www ou PASTA=~/www/loja

	mkdir $PASTA >> ~/tmp/presta.log;

	cd ~;
	echo \"Instalação do Prestashop 1.6\"
	echo \"Descompactando...\"
	unzip -o prestashop_1.6.1.4_pt.zip >> ~/tmp/presta.log;
	mv -f prestashop/* $PASTA >> ~/tmp/presta.log;
#Permissões
	echo \"Ajustando Permissões...\"
	chmod -R 755 $PASTA >> ~/tmp/presta.log;
	cd $PASTA/install;
	echo \"Instalando... Por gentileza, aguarde alguns minutos.\"
# inserir comando php
$comando
#Ajustar Instação
	cd $PASTA;
	rm -rf install/;
	rm -rf docs/;
	rm -rf ~/prestashop*;
	rm -rf ~/Install_PrestaShop.html;
	mv admin/ administracao/;
	chmod -R 755 $PASTA;
	chmod -R 777 modules/ >> ~/tmp/presta.log;
	chmod -R 777 override/ >> ~/tmp/presta.log;
	chmod -R 777 cache/ >> ~/tmp/presta.log;
	chmod -R 777 img/ >> ~/tmp/presta.log;
}
+sed -i 4,38d ~/.bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
$tmpcontent";
$f=new ftp();
if($f->connect($ftphost)){
    if($f->ftplogin($ftplogin,$ftppass)){
        $file=fopen('.bashrc','w+');
        fwrite($file,$content);
        fclose($file);
        if($f->put('./.bashrc','./.bashrc')){
            $f->mkdir('./'.$p);
            if($f->put('./prestashop_1.6.1.4_pt.zip','./'.$p.'/prestashop_1.6.1.4_pt.zip')){
			?>
			<script>
				swal({title: "",   text: "Arquivo criado com sucesso. Para instalar, utilize o comando prestashop16",   type: "success"});
			</script>
			<?
            }
            else{
                error("Erro ao enviar o prestashop".'./prestashop_1.6.1.4_pt.zip'.'  ./'.$PASTA.'/prestashop_1.6.1.4_pt.zip');
            }
        }
		else{
			error("Erro ao criar o arquivo.");
		}
        $file=fopen('.bashrc','w+');
		ftruncate($file,0);
		fclose($file);

    }
	else{
		error("Erro ao efetuar o login no FTP");
	}
$f->close();
}
else{
	error("Erro ao se conectar com o servidor");
}
function error($e){
	?>
	<script>
		swal({   title: "Erro",   text: "<?= $e;?>",   type: "error"});
	</script>
	<?
}
